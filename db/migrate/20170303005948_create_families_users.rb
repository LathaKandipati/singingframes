class CreateFamiliesUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :families_users, :id => false do |t|
      t.integer :family_id
      t.integer :user_id
    end
  end
end
