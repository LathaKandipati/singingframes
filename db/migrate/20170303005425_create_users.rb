class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.text :name
      t.text :email
      t.text :address
      t.string :contact
      t.text :password_digest
      t.text :avatar

      t.timestamps
    end
  end
end
