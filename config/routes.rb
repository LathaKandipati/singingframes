# == Route Map
#
# DEPRECATION WARNING: alias_method_chain is deprecated. Please, use Module#prepend instead. From module, you can access the original method using super. (called from <top (required)> at /Users/deepulatha/Desktop/Desktop/Latha/GA/wdi-19/Projects/SingingFrames/singing_frames/config/environment.rb:5)
# DEPRECATION WARNING: alias_method_chain is deprecated. Please, use Module#prepend instead. From module, you can access the original method using super. (called from <top (required)> at /Users/deepulatha/Desktop/Desktop/Latha/GA/wdi-19/Projects/SingingFrames/singing_frames/config/environment.rb:5)
#      Prefix Verb   URI Pattern                  Controller#Action
#        root GET    /                            session#new
#       users GET    /users(.:format)             users#index
#             POST   /users(.:format)             users#create
#    new_user GET    /users/new(.:format)         users#new
#   edit_user GET    /users/:id/edit(.:format)    users#edit
#        user GET    /users/:id(.:format)         users#show
#             PATCH  /users/:id(.:format)         users#update
#             PUT    /users/:id(.:format)         users#update
#             DELETE /users/:id(.:format)         users#destroy
#    families GET    /families(.:format)          families#index
#             POST   /families(.:format)          families#create
#  new_family GET    /families/new(.:format)      families#new
# edit_family GET    /families/:id/edit(.:format) families#edit
#      family GET    /families/:id(.:format)      families#show
#             PATCH  /families/:id(.:format)      families#update
#             PUT    /families/:id(.:format)      families#update
#             DELETE /families/:id(.:format)      families#destroy
#     invites GET    /invites(.:format)           invites#index
#             POST   /invites(.:format)           invites#create
#  new_invite GET    /invites/new(.:format)       invites#new
# edit_invite GET    /invites/:id/edit(.:format)  invites#edit
#      invite GET    /invites/:id(.:format)       invites#show
#             PATCH  /invites/:id(.:format)       invites#update
#             PUT    /invites/:id(.:format)       invites#update
#             DELETE /invites/:id(.:format)       invites#destroy
#       login GET    /login(.:format)             session#new
#             POST   /login(.:format)             session#create
#             DELETE /login(.:format)             session#destroy
#

Rails.application.routes.draw do

  # Root path
  root :to => 'session#new'

  # CRUD routes for models
  resources :users
  resources :families
  resources :invites

  #Session routes
  get '/login' => 'session#new'
  post '/login' => 'session#create'
  delete '/login' => 'session#destroy'

  # For authentication
  # post 'authenticate', to: 'authentication#authenticate'

end
