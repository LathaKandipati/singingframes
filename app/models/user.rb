# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :text
#  email           :text
#  address         :text
#  contact         :string
#  password_digest :text
#  avatar          :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  admin           :boolean
#  latitude        :float
#  longitude       :float
#

class User < ApplicationRecord
  # Defining relations
  has_and_belongs_to_many :families
  has_many :images
  has_many :posts

  # For sending and receiving invitations
  has_many :invitations, :class_name => "Invite", :foreign_key => 'recipient_id'
  has_many :sent_invites, :class_name => "Invite", :foreign_key => 'sender_id'

  # Defining validations
  has_secure_password
  validates :email, :presence => true, :uniqueness => true
  validates_confirmation_of :password
end
