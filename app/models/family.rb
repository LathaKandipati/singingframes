# == Schema Information
#
# Table name: families
#
#  id         :integer          not null, primary key
#  name       :text
#  photo      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Family < ApplicationRecord
  # Defining relations
  has_and_belongs_to_many :users

  # For sending and receiving invitations
  has_many :invites
end
