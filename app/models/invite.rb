# == Schema Information
#
# Table name: invites
#
#  id           :integer          not null, primary key
#  email        :text
#  family_id    :integer
#  sender_id    :integer
#  recipient_id :integer
#  token        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Invite < ApplicationRecord
  belongs_to :family
  belongs_to :sender, :class_name => 'User'
  belongs_to :recipient, :class_name => 'User', :optional => true

  before_create :generate_token
  before_save :check_user_existence

  def check_user_existence
    puts email
    recipient = User.find_by_email(email)
    if recipient
      self.recipient_id = recipient.id
    end
  end

  def generate_token
    self.token = Digest::SHA1.hexdigest([self.family_id, Time.now, rand].join)
  end
end
