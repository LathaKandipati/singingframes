class InviteMailer < ApplicationMailer

  # From email
  default :from => 'invite.singingframes@gmail.com'

  def new_user_invite(invite, path)
    @invite = invite
    @path = path
    mail :to => @invite.email, :subject => "You are invited to join your family @singingframes.com", :cc => 'invite.singingframes@gmail.com'
  end

  def existing_user_invite(invite)
    @invite = invite
    mail :to => @invite.email, :subject => "You are invited to join your family @singingframes.com", :cc => 'invite.singingframes@gmail.com'
  end

end
