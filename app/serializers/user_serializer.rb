# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :text
#  email           :text
#  address         :text
#  contact         :string
#  password_digest :text
#  avatar          :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  admin           :boolean
#  latitude        :float
#  longitude       :float
#

class UserSerializer < ActiveModel::Serializer

  # Data to be served
  attributes :id, :name, :email, :address, :contact, :avatar, :families, :latitude, :longitude

  # Other related models #Commenting them at the moment as I am trying to include family_ids
  # has_many :families
  # has_many :images
  # has_many :posts

end
