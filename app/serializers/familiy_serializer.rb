class FamiliySerializer < ActiveModel::Serializer

  # Data to be served
  attributes :id, :name, :photo, :user_ids

  # Other related models
  # has_many :users
end
