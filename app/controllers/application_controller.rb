class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception

  respond_to :json
  before_action :fetch_user
  # before_action :authenticate_request

  def location
    if params[:location].blank?
      if Rails.env.test? || Rails.env.development?
        @location ||= Geocoder.search("50.78.167.161").first
      else
        @location ||= request.location
      end
    else
      params[:location].each {|l| l = l.to_i } if params[:location].is_a? Array
      @location ||= Geocoder.search(params[:location]).first
      @location
    end
  end

  private
  # def authenticate_request
  #   @current_user = AuthorizeApiRequest.call(request.headers).result
  #   render json: { error: 'Not Authorized' }, status: 401 unless @current_user
  # end

  def fetch_user
    @current_user = User.find_by :id=>session[:user_id] if session[:user_id].present?
    session[:user_id] = nil unless @current_user.present?
    puts @current_user
  end

  def check_if_admin
    redirect_to login_path unless (@current_user.present? && @current_user.admin?)
  end

  def check_if_logged_in
    redirect_to new_user_path unless @current_user.present?
  end

end
