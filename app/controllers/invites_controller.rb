class InvitesController < ApplicationController
  before_action :check_if_logged_in

  def create
    @invite = Invite.new :family_id=>params[:invite][:family_id], :email=>params[:invite][:email]   # Make a new Invite
    puts @current_user

    @invite.sender_id = @current_user.id # set the sender to the current user

    if !(@current_user.families.include? @invite.family)
      @current_user.families.push(@invite.family)
    end

    if @invite.save
      #if the user already exists
      if @invite.recipient != nil
        #send a notification email
        InviteMailer.existing_user_invite(@invite).deliver_now

        #Add the user to the user group
        if !(@invite.recipient.families.include? @invite.family)
          @invite.recipient.families.push(@invite.family)
        end
      else
        path = "https://singingframes.herokuapp.com/users/new?invite_token=#{@invite.token}"
        InviteMailer.new_user_invite(@invite, path).deliver_now #send the invite data to our mailer to deliver the email
      end
      render :json => {:status => :ok}
    else
      # oh no, creating an new invitation failed
      render :json => {:status => :unprocessable_entity}
    end
  end

end
