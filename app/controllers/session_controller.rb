class SessionController < ApplicationController
  def new
  end

  def create
    user = User.find_by :email=>params[:email]
    # respond_to do |format|
      if (user.present? && user.authenticate(params[:password]))
        session[:user_id] = user.id
        # format.html { redirect_to user, notice: 'User is successfully logged in.' }
        # format.json { render json: user }
        # respond_with(user)
        result = request.location
        puts result.inspect
        if (result.latitude != 0)
          user.latitude = result.latitude
          user.longitude = result.longitude
          user.save
        end
        render json: user
      else
        flash[:error] = "Invalid email or password. Signup if you are new user
                       before making reservations."
        # format.html { render :new }
        # format.json { render json: user.errors, status: :unprocessable_entity }
        # respond_with ()
        errors = "Invalid credentials"
        render json: errors, status: :unprocessable_entity
      end
    # end
  end

  def destroy
    session[:user_id] = nil
    respond_to do |format|
      format.json { render :json =>  {:status => 'ok'} }
      format.html { redirect_to root_path, notice: 'User logged out succefully.' }
    end
  end
end
