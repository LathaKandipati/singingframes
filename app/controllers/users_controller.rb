class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  # before_action :check_if_admin, :only=>[:index]
  # before_action :check_if_logged_in, :only => [:edit, :update, :show, :destroy]

  def index
    @users = User.all
    # render json: User.includes(:families), include: ['families']
    render json: @users
  end

  def show
    # render json: @user, include: ['families']
    render json: @user
  end

  def new
    @user = User.new
    @token = params[:invite_token] #<-- pulls the value from the url query string
  end

  def create
    @user = User.new user_params

    @token = params[:invite_token]
    if @token != nil
      family =  Invite.find_by_token(@token).family #find the user group attached to the invite
      #TBA : check if the @user.email == Invite.email
      #TBA: Add :accepted field to Invite table to and allow existing users the
      #     ability to accept or deny an invitation, instead of being automatically
      #     granted access to the user group.
      #TBA: Set an expiration time on invitations and automatically destroy
      #     invitations after a certain period of time, rendering them unusable.
      @user.families.push(family) #add this user to the new user group as a member
    end

    if params[:file].present?
      req = Cloudinary::Uploader.upload(params[:file])
      @user.image = req["public_id"]
    else
      # @user.image = "http://res.cloudinary.com/dzhoxlq6z/image/upload/v1485418003/New_carousel/home2.jpg"
    end

    # respond_to do |format|
      if @user.save
        session[:user_id] = @user.id
        # format.html { redirect_to root_path, notice: 'User is successfully created.' }
        # format.json { render :json =>  {:status => 'ok'} }
        render json: @user
      else
        flash[:create] = "Account could not be created."
        # render :new
        errors = "Could not create user."
        render json: errors, status: :unprocessable_entity
      end
    # end
  end

  def edit
  end

  def update
    # respond_to do |format|
      if @user.update user_params
        # format.html { redirect_to @user, notice: 'User is successfully updated.' }
        # format.json { render :json, status: :ok}
        render json: @user
      else
        # format.html { render :edit }
        # format.json { render json: @user.errors, status: :unprocessable_entity }
        errors = "Could not update user."
        render json: errors, status: :unprocessable_entity
      end
    # end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation, :email, :address, :contact, :avatar)
  end
end
