class FamiliesController < ApplicationController
  before_action :set_family, only: [:show, :edit, :update, :destroy]
  # before_action :check_if_admin, :only=>[:index]
  # before_action :check_if_logged_in, :only => [:index, :edit, :update, :show, :destroy]

  def index
    @families = Family.all
    render json: @families
  end

  def show
    render json: @family
  end

  def new
    @family = Family.new
  end

  def create
    @family = Family.new family_params
    if params[:file].present?
      req = Cloudinary::Uploader.upload(params[:file])
      @family.photo = req["public_id"]
    else
      # @family.photo = "http://res.cloudinary.com/dzhoxlq6z/image/upload/v1485418003/New_carousel/home2.jpg"
    end

    # respond_to do |format|
      if @family.save
        # format.html { redirect_to families_path, notice: 'Family is successfully updated.' }
        # format.json { render json: @family, status: :ok, location: @family }
        render json: @family
      else
        # format.html { render :new, notice: "Family could not be created." }
        # format.json { render json: @family.errors, status: :unprocessable_entity }
        errors = "Invalid credentials"
        render json: errors, status: :unprocessable_entity
      end
    # end
  end

  def edit
    @invite = Invite.new
  end

  def update
    # respond_to do |format|
      if @family.update family_params
        # format.html { redirect_to @family, notice: 'Family is successfully updated.' }
        # format.json { render :show, status: :ok, location: @family }
        render json: @family
      else
        # format.html { render :edit, notice: "Family could not be created." }
        # format.json { render json: @family.errors, status: :unprocessable_entity }
        errors = "Invalid credentials"
        render json: errors, status: :unprocessable_entity
      end
    # end
  end

  def destroy
    @family.destroy
    respond_to do |format|
      format.html { redirect_to families_path, notice: 'Family is successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_family
    @family = Family.find(params[:id])
  end

  def family_params
    params.require(:family).permit(:name, :photo)
  end
end
